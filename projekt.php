<?php
require_once("venfor/autoload.php");
$latte = new Latte\Engine;
// adresář pro cache
$latte->setTempDirectory('Temp');

// to do code
$a = 3;
$b = 4;
$c = $a + $b;

$params = [ 
    'vysledek' -> $c,
];
// or $params = new TemplateParameters(/* ... */);

// kresli na výstup
$latte->render('template.latte', $params);
// kresli do proměnné
$output = $latte->renderToString('template.latte', $params);
?>